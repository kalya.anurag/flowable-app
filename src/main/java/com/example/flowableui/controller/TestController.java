package com.example.flowableui.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.example.flowableui.model.DetailsList;
import com.example.flowableui.model.SalaryRequest;
import com.example.flowableui.model.TaskDetails;

@Controller
@RequestMapping("/salary")
@CrossOrigin
public class TestController {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@RequestMapping(value = "/applySalary", method = RequestMethod.GET)
	public String showapplyLeaves(ModelMap model) {
		System.out.println(jdbcTemplate);
		List<String> listOfEmpCodes = jdbcTemplate.queryForList("SELECT empCode from empmaster where empDDOId = ? ",
				new Object[] { 1 }, String.class);
		System.out.println(listOfEmpCodes);
		model.put("empCodes", listOfEmpCodes);
		return "applySalary";
	}

	@RequestMapping(value = "/salaryRequest", method = RequestMethod.POST)
	public void submitLeaves(ModelMap model, @RequestBody SalaryRequest request, HttpServletRequest req,
			HttpServletResponse response) {

		System.out.println("IN POST SALARY REQUEST:" + request.getRequestDescription() + " - " + request.getDdoId()
				+ " - " + request.getEmpCode() + " - " + request.getEmpGrade() + " - " + request.getEmpName() + " - "
				+ request.getLastDrawnSalary());
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<SalaryRequest> entity = new HttpEntity<SalaryRequest>(request, httpHeaders);

		try {
			ResponseEntity<Object> responseEntity = restTemplate.exchange(
					"http://localhost:8080/flowable-service/salary/apply", HttpMethod.POST, entity, Object.class);
		} catch (HttpStatusCodeException e) {
			HttpStatus status = e.getStatusCode();
			if (status == HttpStatus.BAD_REQUEST) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			} else {
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/getManagerTasks", method = RequestMethod.GET)
	public String getTasks(ModelMap model) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity entity = new HttpEntity(httpHeaders);
		List<DetailsList> outputList = new ArrayList<DetailsList>();

		ResponseEntity<List<TaskDetails>> responseEntity = restTemplate.exchange(
				"http://localhost:8080/flowable-service/manager/tasks", HttpMethod.GET, entity,
				new ParameterizedTypeReference<List<TaskDetails>>() {
				});
		List<TaskDetails> list = (List<TaskDetails>) responseEntity.getBody();

		DetailsList obj;
		for (TaskDetails taskDetails : list) {
			obj = new DetailsList();
			obj.setTaskId(taskDetails.getTaskId());
			obj.setTaskName(taskDetails.getTaskName());
			obj.setEmployeeName((String) taskDetails.getTaskData().get("employeeName"));
			obj.setEmployeeCode((String) taskDetails.getTaskData().get("employeeCode"));
			obj.setLastDrawnSalary((String) taskDetails.getTaskData().get("lastDrawnSalary"));
			obj.setDescription((String) taskDetails.getTaskData().get("description"));

			outputList.add(obj);
		}

		model.put("listOfTasks", outputList);
		return "viewTasks";
	}

	@RequestMapping(value = "/getUserTasks", method = RequestMethod.GET)
	public String getUserTasks(ModelMap model) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity entity = new HttpEntity(httpHeaders);
		List<DetailsList> outputList = new ArrayList<DetailsList>();

		ResponseEntity<List<TaskDetails>> responseEntity = restTemplate.exchange(
				"http://localhost:8080/flowable-service/user/tasks", HttpMethod.GET, entity,
				new ParameterizedTypeReference<List<TaskDetails>>() {
				});
		List<TaskDetails> list = (List<TaskDetails>) responseEntity.getBody();

		DetailsList obj;
		for (TaskDetails taskDetails : list) {
			obj = new DetailsList();
			obj.setTaskId(taskDetails.getTaskId());
			if (taskDetails.getTaskName().equalsIgnoreCase("Approve or reject request")) {
				obj.setTaskName("Request Pending");
			} else {
				obj.setTaskName(taskDetails.getTaskName());
			}
			obj.setEmployeeName((String) taskDetails.getTaskData().get("employeeName"));
			obj.setEmployeeCode((String) taskDetails.getTaskData().get("employeeCode"));
			obj.setLastDrawnSalary((String) taskDetails.getTaskData().get("lastDrawnSalary"));
			obj.setDescription((String) taskDetails.getTaskData().get("description"));

			outputList.add(obj);
		}

		model.put("listOfTasks", outputList);
		return "viewUserTasks";
	}

	@RequestMapping(value = "/approveReject", method = RequestMethod.POST)
	public void approveOrReject(ModelMap model, @RequestParam(name = "taskId") String taskId,
			@RequestParam(name = "flag") String flag, HttpServletResponse response) {
		HttpHeaders httpHeaders = new HttpHeaders();
		HttpEntity entity = new HttpEntity(httpHeaders);
		ResponseEntity<Object> responseEntity = null;
		try {
			String url = "http://localhost:8080/flowable-service/manager/approve/tasks/" + taskId.trim();
			if (flag.equalsIgnoreCase("Y")) {
				url = url + "/true";
			} else if (flag.equalsIgnoreCase("N")) {
				url = url + "/false";
			}
			responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, Object.class);
		} catch (HttpStatusCodeException e) {
			HttpStatus status = e.getStatusCode();
			if (status == HttpStatus.BAD_REQUEST) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			} else {
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}

	}
}
