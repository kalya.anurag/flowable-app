package com.example.flowableui.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DetailsList {

	String taskId;
	String taskName;
	String employeeName;
	String employeeCode;
	String lastDrawnSalary;
	String description;

	public DetailsList() {
	}

}
