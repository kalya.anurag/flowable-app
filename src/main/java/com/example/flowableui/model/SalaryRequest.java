package com.example.flowableui.model;

public class SalaryRequest {

	String ddoId;
	String empCode;
	String empName;
	String empGrade;
	String lastDrawnSalary;
	String requestDescription;

	public SalaryRequest() {
		super();
	}

	public String getDdoId() {
		return ddoId;
	}

	public void setDdoId(String ddoId) {
		this.ddoId = ddoId;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpGrade() {
		return empGrade;
	}

	public void setEmpGrade(String empGrade) {
		this.empGrade = empGrade;
	}

	public String getLastDrawnSalary() {
		return lastDrawnSalary;
	}

	public void setLastDrawnSalary(String lastDrawnSalary) {
		this.lastDrawnSalary = lastDrawnSalary;
	}

	public String getRequestDescription() {
		return requestDescription;
	}

	public void setRequestDescription(String requestDescription) {
		this.requestDescription = requestDescription;
	}

}