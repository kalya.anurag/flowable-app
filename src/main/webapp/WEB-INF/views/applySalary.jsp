<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Flowable Web Application</title>

<!-- Icons font CSS-->
<link
	href="${contextPath }/resources/vendor/mdi-font/css/material-design-iconic-font.min.css"
	rel="stylesheet" media="all">
<link
	href="${contextPath }/resources/vendor/font-awesome-4.7/css/font-awesome.min.css"
	rel="stylesheet" media="all">
<!-- Font special for pages-->
<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">

<!-- Vendor CSS-->
<link href="${contextPath }/resources/vendor/select2/select2.min.css"
	rel="stylesheet" media="all">

<!-- Material Design for Bootstrap CSS -->
<link rel="stylesheet"
	href="${contextPath }/resources/css/bootstrap-material-design.css" />
<link rel="stylesheet"
	href="${contextPath }/resources/css/jquery-ui.css" />
<!-- <link rel="stylesheet" href="${contextPath }/resources/css/bootstrap.min.css" />-->
<link rel="stylesheet"
	href="${contextPath }/resources/css/typography.css" />
<!-- Main CSS-->
<link href="${contextPath }/resources/css/main.css" rel="stylesheet"
	media="all">
</head>

<body>
	<h3 style="text-align: right;">Sanjeev Kumar (DDO)</h3>
	<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
		<div class="wrapper wrapper--w680">
			<div class="card card-4">
				<div class="card-body">
					<h2 class="title">
						<b>IFMS 3.0 WORKFLOW TOOL POC</b>
					</h2>
					<h2 class="title">Post Salary Request</h2>
					<form name="salaryForm" id="salaryForm" method="post">
						<div class="input-group">
						<label class="label">Employee Code</label>
							<%-- <select id="empCode" name="empCode" class="form-control">
								<option value="" selected>Select Employee Code</option>
								<c:forEach var="empCode" items="${empCodes}">
									<option value="${empCode}">${empCode}</option>
								</c:forEach>
							</select> --%>
							<input class="input--style-4" type="text" name="empCode"
								id="empCode" placeholder="Enter Code">
						</div>
						<div class="input-group">
							<label class="label">Employee Name</label> <input
								class="input--style-4" type="text" name="empName" id="empName"
								placeholder="Enter Name">
						</div>
						<div class="input-group">
							<label class="label">Employee Grade</label> <input
								class="input--style-4" type="text" name="empGrade" id="empGrade"
								placeholder="Enter Grade">
						</div>
						<div class="input-group">
							<label class="label">Last Drawn Salary</label> <input
								class="input--style-4" type="text" name="lastDrawnSalary"
								id="lastDrawnSalary" placeholder="Enter Last Salary">
						</div>
						<div class="input-group">
							<label class="label">Description</label> <input
								class="input--style-4" type="text" name="requestDescription"
								id="requestDescription" placeholder="Enter Description">
						</div>

						<div class="p-t-15">
							<button type="button" class="btn btn--radius-2 btn--blue"
								onClick="submitLeave()">Submit</button>
						
							<button type="button" class="btn btn--radius-2 btn--blue" style="background-color: lightsteelblue;"
								onClick="getTasks('${contextPath }/salary/getUserTasks')">Get
								Tasks</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- </form> -->
	<!-- Jquery JS-->
	<script src="${contextPath }/resources/vendor/jquery/jquery.min.js"></script>
	<!-- Vendor JS-->
	<script src="${contextPath }/resources/vendor/select2/select2.min.js"></script>
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/bootstrap.min.js"></script>
	<script src="${contextPath }/resources/js/bootstrap-material-design.js"></script>
	<script type="text/javascript"
		src="${contextPath }/resources/js/jquery-ui.js"></script>

	<script>
		function submitLeave() {
			var jsonResponse = {};
			jsonResponse.ddoId = "DDO1";
			jsonResponse.empCode = $("#empCode").val();
			jsonResponse.empName = $("#empName").val();
			jsonResponse.empGrade = $("#empGrade").val();
			jsonResponse.lastDrawnSalary = $("#lastDrawnSalary").val();
			jsonResponse.requestDescription = $("#requestDescription").val();
			$
					.ajax({
						type : 'POST',
						url : "salaryRequest",
						//dataType: 'text',
						data : JSON.stringify(jsonResponse),
						contentType : "application/json; charset=utf-8",
						success : function(result, success, jqXHR) {
							var ct = jqXHR.getResponseHeader('content-type')
									|| "";
							if (ct.indexOf('text/html') >= 0) {
								var errorString = 'Something went wrong. Please refresh and try again';
								alert(errorString);
							} else {
								var successString = 'Salary Applied Successfully';
								alert(successString);
							}
						},
						error : function() {
							var errorString = 'Something went wrong. Please refresh and try again';
							alert(errorString);
						}
					});
		}
		function getTasks(url) {
			window.location.href = url;
		}
	</script>
</body>

</html>