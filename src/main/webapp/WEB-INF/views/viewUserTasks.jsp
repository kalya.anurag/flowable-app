<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Flowable Web Application</title>

<!-- Icons font CSS-->
<link
	href="${contextPath }/resources/vendor/mdi-font/css/material-design-iconic-font.min.css"
	rel="stylesheet" media="all">
<link
	href="${contextPath }/resources/vendor/font-awesome-4.7/css/font-awesome.min.css"
	rel="stylesheet" media="all">
<!-- Font special for pages-->
<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">

<!-- Vendor CSS-->
<link href="${contextPath }/resources/vendor/select2/select2.min.css"
	rel="stylesheet" media="all">

<!-- Material Design for Bootstrap CSS -->
<link rel="stylesheet"
	href="${contextPath }/resources/css/bootstrap-material-design.css" />
<link rel="stylesheet"
	href="${contextPath }/resources/css/jquery-ui.css" />
<!-- <link rel="stylesheet" href="${contextPath }/resources/css/bootstrap.min.css" />-->
<link rel="stylesheet"
	href="${contextPath }/resources/css/typography.css" />
<!-- Main CSS-->
<link href="${contextPath }/resources/css/main.css" rel="stylesheet"
	media="all">
</head>

<body>
	<h3 style="text-align: right;">Sanjeev Kumar (DDO)</h3>
	<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
		<div class="wrapper wrapper--w1200">
			<div class="card card-4">
				<div class="card-body">
					<h2 class="title">List Of Tasks</h2>
					<table id="taskMaintenance" class="table table-striped table-dark">
						<thead>
							<tr>
								<th scope="col" style="color: white;">Task ID</th>
								<th scope="col" style="color: white;">Employee Code</th>
								<th scope="col" style="color: white;">Employee Name</th>
								<th scope="col" style="color: white;">Last Salary</th>
								<th scope="col" style="color: white;">Description</th>
								<th scope="col" style="color: white;">Task Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listOfTasks }" var="task">
								<tr>
									<td>${task.taskId }</td>
									<td>${task.employeeCode }</td>
									<td>${task.employeeName }</td>
									<td>${task.lastDrawnSalary }</td>
									<td>${task.description }</td>
									<td style="background-color: #8a978a;">${task.taskName }</td>
								</tr>
							</c:forEach>
						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Jquery JS-->
	<script src="${contextPath }/resources/vendor/jquery/jquery.min.js"></script>
	<!-- Vendor JS-->
	<script src="${contextPath }/resources/vendor/select2/select2.min.js"></script>
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript"
		src="${contextPath }/resources/js/bootstrap.min.js"></script>
	<script src="${contextPath }/resources/js/bootstrap-material-design.js"></script>
	<script type="text/javascript"
		src="${contextPath }/resources/js/jquery-ui.js"></script>
</body>

</html>